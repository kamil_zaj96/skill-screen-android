package com.letitscore.githubsearch.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.letitscore.githubsearch.R
import com.letitscore.githubsearch.model.GithubRepo


class GithubRepoAdapter(private val data: List<GithubRepo>) :
    RecyclerView.Adapter<GithubRepoAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_search, parent, false)
        return ViewHolder(view)
    }


    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    }
}
