package com.letitscore.githubsearch.ui.base

import androidx.lifecycle.ViewModel
import com.letitscore.githubsearch.data.ApiManager
import com.letitscore.githubsearch.data.DataSource

abstract class BaseViewModel: ViewModel() {
    val dataSource: DataSource = ApiManager.instance
}
