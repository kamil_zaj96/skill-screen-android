package com.letitscore.githubsearch.ui

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.letitscore.githubsearch.R
import com.letitscore.githubsearch.BR.*
import com.letitscore.githubsearch.databinding.ActivityMainBinding
import com.letitscore.githubsearch.model.GithubRepo
import com.letitscore.githubsearch.ui.base.BaseActivity
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity<ActivityMainBinding, MainVM>() {
    override fun getViewModel(): MainVM = ViewModelProvider(this).get(MainVM::class.java)

    private val disposables: CompositeDisposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val viewDataBinding: ActivityMainBinding = DataBindingUtil.setContentView(
            this,
            R.layout.activity_main
        )
        viewDataBinding.lifecycleOwner = this
        viewDataBinding.setVariable(vm, getViewModel())
        viewDataBinding.executePendingBindings()
    }

    private fun initializeData(data: List<GithubRepo>) {
        val adapter = GithubRepoAdapter(data)
        activity_main_main_list.adapter = adapter
    }
}
