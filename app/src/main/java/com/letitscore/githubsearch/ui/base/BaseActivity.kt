package com.letitscore.githubsearch.ui.base

import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.ViewDataBinding

abstract class BaseActivity<B: ViewDataBinding, V: BaseViewModel>: AppCompatActivity() {
    abstract fun getViewModel(): V
}
