package com.letitscore.githubsearch.data

object ApiConfig {
    const val BASE_URL = "https://api.github.com"
}
