package com.letitscore.githubsearch.data

import com.letitscore.githubsearch.model.GithubRepo
import io.reactivex.Single

interface DataSource {
    fun searchRepositories(query: String): Single<List<GithubRepo>>
}
