package com.letitscore.githubsearch.data

import com.letitscore.githubsearch.model.GithubRepo
import io.reactivex.Single
import retrofit2.*
import retrofit2.converter.gson.GsonConverterFactory
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.OkHttpClient
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory


class ApiManager private constructor(): DataSource {

    companion object {
        val instance = ApiManager()
    }

    private val retrofit: Retrofit
    private val baseService: BaseService

    init {
        val logging = HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BODY
        }

        val client = OkHttpClient.Builder()
            .addInterceptor(logging)
            .build()

        retrofit = Retrofit.Builder()
            .baseUrl(ApiConfig.BASE_URL)
            .client(client)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        baseService = retrofit.create()
    }

    //region DataSource
    override fun searchRepositories(query: String): Single<List<GithubRepo>> {
        //TODO
        return Single.never()
    }
    //endregion
}
