package com.letitscore.githubsearch.data

import com.letitscore.githubsearch.model.GithubRepo

data class SearchRepositoriesResponse(val items: List<GithubRepo>)
